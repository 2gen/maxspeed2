(function (global) {
    var app = global.app = global.app || {};
    global.settings = {};

    document.addEventListener("deviceready", function () {
        app.application = new kendo.mobile.Application(document.body, { layout: "tabstrip-layout" });
        app.main.onLoadSettings();
        app.main.onWatch(settings);
        navigator.splashscreen.hide();        
    }, false);
    
    app.main = {
        onReset: function() {            
            var $oldSpeed = $("#currentMax");
            $oldSpeed.html("0");
            if (settings.settingViberation == "true") navigator.notification.vibrate(100);
        },
        
        onWatch: function() {
        	var watchOptions = {
        		frequency: 1500,
        		enableHighAccuracy: true
        	};
            var watchID = navigator.geolocation.watchPosition(onGEOSuccess, onGEOError, watchOptions);
            localStorage.setItem("settingWatchID", watchID);
            settings.settingWatchID = watchID;
        },
        
        onLoadSettings: function() {
            global.settings = {
                settingViberation: localStorage.getItem("settingViberation"),
                settingBeep: localStorage.getItem("settingBeep"),
                settingNotification: localStorage.getItem("settingNotification"),
                settingBackground: localStorage.getItem("settingBackground"),
                settingWatchID: localStorage.getItem("settingWatchID")
            };
            
            if (settings.settingNotification != "true") {
                window.plugins.statusBarNotification.clear();
            }
        }
    }
    
})(window)



function onGEOSuccess(position) {
/*
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + position.timestamp                + '\n');
*/
    //var errorIcon = $(".header .error");
    //alert(settings.settingViberation);
    var $oldSpeed = $("#currentMax");
    var oldSpeed = $oldSpeed.html();
    var newSpeed = position.coords.speed;
    var newSpeedKM = newSpeed/1000*60*60;
    var newSpeedKMr = Math.round(newSpeedKM * 10) / 10;
    if (isNaN(oldSpeed)) oldSpeed = 0;
    if (newSpeedKMr > oldSpeed) {
        $oldSpeed.html(newSpeedKMr);
        if (settings.settingViberation == "true") navigator.notification.vibrate(100);
        if (settings.settingBeep == "true") navigator.notification.beep(1);
        if (settings.settingNotification == "true") window.plugins.statusBarNotification.notify("Maxspeed 2", "New Max Speed: " + newSpeedKMr + " KM/hr");
    }
    //errorIcon.hide();
}

function onGEOError(error) {
    //var errorIcon = $(".header .error");
    //errorIcon.show();
}