(function (global) {
    var SettingsViewModel,
        app = global.app = global.app || {};

    SettingsViewModel = kendo.data.ObservableObject.extend({
        onChange: function(e) {
            var thisSetting = e.sender.element.data("settingname");
            localStorage.setItem(thisSetting, e.checked);
            app.main.onLoadSettings();
        }
    });

    app.settingsService = {
        onInit: function () {
            var _switchViberation = $("#switchViberation").data("kendoMobileSwitch"),
                _switchNotification = $("#switchNotification").data("kendoMobileSwitch"),
                _switchBackground = $("#switchBackground").data("kendoMobileSwitch"),
                _switchBeep = $("#switchBeep").data("kendoMobileSwitch");

            if (settings.settingViberation == "true") {
                _switchViberation.check(true);
            }else{
                _switchViberation.check(false);
            }
            
            if (settings.settingBeep == "true") {
                _switchBeep.check(true);
            }else{
                _switchBeep.check(false);
            }
            
            if (settings.settingNotification == "true") { 
                _switchNotification.check(true);
            }else{
                _switchNotification.check(false);
            }

            if (settings.settingBackground == "true") {
                _switchBackground.check(true);
            }else{
                _switchBackground.check(false);
            }

        },
        viewModel: new SettingsViewModel()
    };
})(window);