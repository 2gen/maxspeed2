(function (global) {
    var QuitViewModel,
        app = global.app = global.app || {};

    QuitViewModel = kendo.data.ObservableObject.extend({
        onQuit: function() {
            var that = this;
            localStorage.setItem("settingWatchID", 0);
            navigator.geolocation.clearWatch(settings.settingWatchID);
            navigator.app.exitApp();
        }
    });

    app.quitService = {
        viewModel: new QuitViewModel()
    };
})(window);